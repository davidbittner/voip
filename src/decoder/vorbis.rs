use std::io::Cursor;
use super::Decoder;

pub struct Vorbis {
    buffer: Vec<u8>,
}

impl Vorbis {
    pub fn new() -> Vorbis {
        Vorbis {
            buffer: Vec::new(),
        }
    }

}

impl Decoder for Vorbis {
    type ErrType = vorbis::VorbisError;
    type RetType = Result<Vec<i16>, Self::ErrType>;

    fn append<I>(&mut self, with: I)
        where
            I: IntoIterator<Item = u8> {
        self.buffer.extend(with);
    }

    fn decode(&mut self) -> Self::RetType {
        let mut buffer = Vec::new();
        std::mem::swap(&mut self.buffer, &mut buffer);

        let cursor = Cursor::new(buffer);
        //Remove chunks that failed to decode, pull out the data
        //and then collect that into a vector of vectors.
        //Finally, flatten that to end up with a single vector
        let temp: Vec<Vec<i16>> = vorbis::Decoder::new(cursor)?
            .into_packets()
            .filter_map(Result::ok)
            .map(|packet| packet.data)
            .collect();

        Ok(temp.into_iter().flatten().collect())
    }
}
