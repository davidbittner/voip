pub mod vorbis;

pub trait Decoder {
    type ErrType: std::error::Error;
    type RetType = Result<Vec<i16>, Self::ErrType>;

    fn append<I>(&mut self, with: I)
        where
            I: IntoIterator<Item = u8>;

    fn decode(&mut self) -> Self::RetType;
}
