use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct User {
    name: String,
    desc: String,
    id:   Uuid,
}

impl User {
    pub fn new_user(name: String, desc: String) -> User {
        User {
            name: name,
            desc: desc,
            id:   Uuid::new_v4()
        }
    }
}
