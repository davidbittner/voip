#![feature(associated_type_defaults)]
#![forbid(unsafe_code)]

pub mod packet;
pub mod encoder;
pub mod decoder;
pub mod user;
pub mod room;

#[cfg(test)]
mod tests {
    #[test]
    fn temp() {
    }
}
