use serde::{Serialize, Deserialize};
use uuid::Uuid;
use crate::user::User;
use crate::room::{Room, RoomId};

pub type UserId = Uuid;

#[derive(Serialize, Deserialize, Debug)]
pub enum Packet {
    Voice          (Vec<u8>),
    ChatMsg        (Room, String),
    RequestJoin    (Room),
    LeaveRoom      (RoomId),
    UserJoined     (User, Room),
    SuccessfulJoin (Room),
}
