use crate::user::User;

use uuid::Uuid;
use serde::{Serialize, Deserialize};

pub type RoomId = Uuid;

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct Room {
    pub name:  String,
    pub users: Vec<User>,
    pub id: RoomId,
    pub pass: Option<String>
}

impl Room {
    pub fn new() -> Self {
        Room::default()
    }

    pub fn gen_uuid(mut self) -> Self {
        self.id = Uuid::new_v4(); 
        self
    }

    pub fn give_uuid(mut self, uuid: Uuid) -> Self {
        self.id = uuid;
        self
    }

    pub fn strip_pass(mut self) -> Self {
        self.pass = None;
        self
    }
}
