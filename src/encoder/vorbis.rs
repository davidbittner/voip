use super::Encoder;
use vorbis::VorbisQuality;

pub struct Vorbis{
    encoder: vorbis::Encoder,
}

impl Vorbis {
    pub fn new(channels: u8, rate: u64, quality: VorbisQuality) -> Result<Self, vorbis::VorbisError> {
        Ok(Vorbis{
            encoder: vorbis::Encoder::new(channels, rate, quality)?,
        })
    }
}

impl Encoder for Vorbis {
    type ErrType = vorbis::VorbisError;
    type RetType = Result<Vec<u8>, Self::ErrType>;

    fn encode(&mut self, dat: &Vec<i16>) -> Self::RetType {
        self.encoder.encode(dat)
    }
}
