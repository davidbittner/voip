pub mod vorbis;

pub trait Encoder {
    type ErrType: std::error::Error;
    type RetType = Result<Vec<u8>, Self::ErrType>;

    fn encode(&mut self, dat: &Vec<i16>) -> Self::RetType;
}
